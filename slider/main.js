(function() {
    var imagesList = [
        'imgs/image1.jpg',
        'imgs/image2.jpg',
        'imgs/image3.jpg',
        'imgs/image4.jpg',
        'imgs/image5.jpg',
        'imgs/image6.jpg'
    ];
    var current_index = 0;
    var newImg;
    var timer;
    // Execute only once on page is loaded
    function onInit() {

        //create image
        newImg = document.createElement('img');
        newImg.setAttribute('src', imagesList[current_index]);
        var slider =  document.getElementById('slider');
        slider.appendChild(newImg);

        //create previous button
        var previousBut = document.createElement('button');
        previousBut.innerHTML = 'Prev';
        previousBut.onclick = previousSlide;
        slider.appendChild(previousBut);

        //create next button
        var nextBut = document.createElement('sec_button');
        nextBut.innerHTML = 'Next';
        nextBut.onclick = nextSlide;
        slider.appendChild(nextBut);

        //create timer
        timer = setInterval(nextSlide, 5000);
       
    }

    // Execute on next button click
    function nextSlide() {
        current_index++;
        if(current_index > imagesList.length - 1){
            current_index = 0;
        }
        console.log(current_index, imagesList.length)
        newImg.setAttribute('src', imagesList[current_index]);
        
        /*Urmatoarele 2 linii permit: cand apasam butonul next se sterge timer-ul
        si se creeaza unul nou
        */
        clearInterval(timer)
        timer = setInterval(nextSlide, 5000);
    }

    // Execute on previous button click
    function previousSlide() {
        current_index--;
        if(current_index < 0){
            current_index = imagesList.length - 1;
        }
        console.log(current_index, imagesList.length)//este optional, o folosim cand dorim sa vedem cum lucreaza in consola
        newImg.setAttribute('src', imagesList[current_index]);
    }

    document.addEventListener('DOMContentLoaded' , onInit )//permite afisarea imaginii odata ce continutul este incarcat in browser
    
})();
