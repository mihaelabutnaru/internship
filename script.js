var randomNumber = Math.floor(Math.random()*100)+1;

var guesses = document.querySelector('.guesses');//guesses sunt incercarile(numerele presupuse)
var lastResult =  document.querySelector('.lastResult');//rezultatul care apare la sfarsit
var lowOrHigh = document.querySelector('.lowOrHigh');//lowOrHigh este mic sau mare(numarul presupus)

var guessSubmit = document.querySelector('.guessSubmit');//trimite numarul presupus
var guessField = document.querySelector('.guessField');

var guessCount=1;//numarul de incercari
var resetButton;
function checkGuess(){
	var userGuess = Number(guessField.value);//se declara variabila userGuess  care se ruleaza cu ajutorul lui Number, il folosim pentru a sti exact ca se cer doar numere de introdus
	if(guessCount === 1){//daca numarul de incercari este egal cu 1
		guesses.textContent = 'Previous guesses: ';//Incercarile anterioare
	}
	guesses.textContent += userGuess + ' ';//este echivalent cu guesses.textContent = guesses.textContent + userGuess + '';
	//adica Previous guesses: 5 6 8 (exemplu)
	
	if(userGuess === randomNumber){
		lastResult.textContent = 'Congratulations, you win!';
		lastResult.style.backgroundColor = 'green';
		lowOrHigh.textContent = ' ';
		setGameOver();
	}else if(guessCount === 10){
		lastResult.textContent = 'Game OVER!!';
		setGameOver();
	}else{
		lastResult.textContent = 'Wrong!!';
		lastResult.style.backgroundColor = 'red';
		if(userGuess < randomNumber){
			lowOrHigh.textContent = 'Last guess was too low!';
		}else if(userGuess > randomNumber){
			lowOrHigh.textContent = 'Last guess was too high!';
		}
	}
	guessCount++;
	guessField.value = '';
	guessField.focus();
	
}
guessSubmit.addEventListener('click', checkGuess);//adaugam un click eventListener, ai cand se face click pe submit se va apela functia checkGuess

function setGameOver(){
	guessField.disabled = true;//dezactiveaza campul pentru adaugare
	guessSubmit.disabled = true;//dezactiveaza butonul submit
	//odata ce este deja game over jucatorul numai trebuie sa aiba posibilitatea de a adauga vre-un numar
	
	resetButton = document.createElement('button');//creaza butonul de Resetare
	resetButton.textContent = 'Start new game!';//ii atribuie textul "Start new game!"
	document.body.appendChild(resetButton);//ataseaza butonul la sfarsitul codului HTML
	resetButton.addEventListener('click', resetGame);//adaugam click event pentru cand vom apasa buton de resetare sa se apeleze functia resetGame
}

function resetGame(){
	guessCount = 1;
	
	var resetParas = document.querySelectorAll('.resultParas p');
	for(var i = 0; i < resetParas.length; i++){
	 resetParas[i].textContent = ' ';
	}
	
	resetButton.parentNode.removeChild(resetButton);
	
	guessField.disabled = false;
	guessSubmit.disabled = false;
	guessField.value = ' ';
	guessField.focus();
	
	lastResult.style.backgroundColor = 'white';
	
	randomNumber = Math.floor(Math.random() * 100) + 1;//floor se foloseste pentru a rotunji numarul in jos pana la intreg
	}
